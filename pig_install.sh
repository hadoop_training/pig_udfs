#!/bin/bash
echo 'Pig installation'
rm -rf pig-0.17.0*
apt-get update && apt-get install -y wget ant
wget 'http://apache.org/dist/pig/pig-0.17.0/pig-0.17.0.tar.gz'
tar zxvf pig-0.17.0.tar.gz
cd pig-0.17.0
ant clean
ant
working_dir=`pwd`
path="export PATH=/${working_dir}/bin:$PATH"
export ${path}
echo ${path} >> ~/.bashrc
pig -version
pig_home="PIG_HOME=/${working_dir}"
export ${pig_home}
echo ${pig_home} >> ~/.bashrc
cd tutorial/
ant
cp pigtutorial.tar.gz ../../
cd ../../
tar zxvf pigtutorial.tar.gz
